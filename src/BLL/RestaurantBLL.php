<?php
/**
 * Created by PhpStorm.
 * User: tonid
 * Date: 22/02/2019
 * Time: 15:50
 */

namespace App\BLL;


use App\Entity\Restaurant;

use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;

class RestaurantBLL  extends BaseBLL
{

    public function nueva(string $nombre,string $description,
        string $daysopen, string $cuisine, string $phone, string $imagen,
                          Request $request,
                          $avatars_directory, $url_avatars_directory
    )
    {

        $restaurant = new Restaurant();
        $restaurant->setName($nombre);
        $restaurant->setDescription($description);
        $restaurant->setDaysOpen($daysopen);
        $restaurant->setCuisine($cuisine);
        $restaurant->setPhone($phone);
        $restaurant->setImage($imagen);
        $user  =$this->getUser();
        $restaurant->setOwner($user);
        $restaurant->setImage($this->subirImagen($request, $imagen, $avatars_directory, $url_avatars_directory));

        $restaurant->setPublishAt(new \DateTime());

        return $this->guardaValidando($restaurant);
    }

    public function subirImagen(
        Request $request,
        $avatar,
        string $avatars_directory,
        string $url_avatars_directory)
    {
        $arr_avatar = explode(',', $avatar);

        if (count($arr_avatar) < 2)
            throw new BadRequestHttpException('formato de imagen incorrecto');

        $imgAvatar = base64_decode($arr_avatar[1]);
        if (!is_null($imgAvatar))
        {
            $fileName = 'restaurant-'.time().'.jpg';
            $filePath = $url_avatars_directory . $fileName;
            $urlAvatar = $request->getUriForPath($filePath);
            $ifp = fopen($avatars_directory . $fileName, "wb");
            if ($ifp)
            {
                $ok = fwrite($ifp, $imgAvatar);
                if ($ok)
                {
                    fclose($ifp);

                    return $urlAvatar;
                }
            }
        }

        return '/uploads/default.jpg';
    }

    public function getRestaurantesFiltrados( string $name=null, string $numberStar=null,
                                              string $open=null, string $active=null , string $order = null){

        $tareas = $this->em->getRepository(Restaurant::class)
            ->getRestaurantFiltrados(
                $name, $numberStar, $open, $active,$order);

        return $this->entitiesToArray($tareas);

    }

    public function update(Restaurant $restaurant, array $data,
                           Request $request,
                           $avatars_directory, $url_avatars_directory)
    {

        $restaurant->setName($data['name']);
        $restaurant->setDescription($data['description']);
        $restaurant->setDaysOpen($data['daysOpen']);
        $restaurant->setCuisine($data['cuisine']);
        $restaurant->setPhone($data['phone']);
        $restaurant->setImage($this->subirImagen($request, $data['image'], $avatars_directory, $url_avatars_directory));


        return $this->guardaValidando($restaurant);
    }

    public function delete(Restaurant $restaurant)
    {
        try {
            $this->deleteEntity($restaurant);
            return [
                'error'=>false,
                'message'=>'Operation sucesfull , entity deleteded'];
        }
        catch (\Exception $exception){
            return [
                'error'=>true,
                'message'=>$exception->getMessage()];
        }
    }

    public function getRestaurantOwner(User $user = null){
        if($user == null){
            $user  =$this->getUser();
        }

        $tareas = $this->em->getRepository(Restaurant::class)->findBy(['owner'=>$user]);
        return $this->entitiesToArray($tareas);
    }

    public function toArray($entity)
    {
        if (is_null($entity))
            return null;

        if (!($entity instanceof Restaurant))
            throw new Exception("La entidad no es una Tarea");

        return [
            'id' => $entity->getId(),
            'nombre' => $entity->getName(),
            '$daysOpen' => $entity->getDaysOpen(),
            'cuisine' => $entity->getCuisine(),
            'phone' => $entity->getPhone(),
            'imagen' => $entity->getImage(),
            'publishAt'=> $entity->getPublishAt()->format("d-m-Y H:i:s"),
            'numberStars'=>$entity->getStars(),
            'owner'=>$entity->getOwner()->getId()
        ];
    }
}