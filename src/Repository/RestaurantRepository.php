<?php
/**
 * Created by PhpStorm.
 * User: tonid
 * Date: 22/02/2019
 * Time: 15:41
 */

namespace App\Repository;

use App\Entity\Restaurant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\ORM\QueryBuilder;
/**
 * @method Restaurant|null find($id, $lockMode = null, $lockVersion = null)
 * @method Restaurant|null findOneBy(array $criteria, array $orderBy = null)
 * @method Restaurant[]    findAll()
 * @method Restaurant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RestaurantRepository  extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Restaurant::class);
    }

    public function getRestaurantFiltrados( $name = null ,  $numberstar = null ,
         $open = null  ,  $active = "activate" ,   $order = 'publishAt'  ){
        $qb = $this->createQueryBuilder('r')
            ->orderBy('r.'.$order, 'DESC');


        if (isset($open)){
            $dayofweek = date('w', time())."";
            $qb->andWhere('r.daysOpen LIKE  :val1')
                ->setParameter('val1','%'.$dayofweek.'%');
        }

        if(isset($numberstar)){
            $qb->andWhere('r.stars = :val3')
                ->setParameter('val3',$numberstar);
        }

        if(isset($name)){
            $qb->andWhere('r.name LIKE :term OR r.cuisine LIKE :term')
                ->setParameter('term','%'.$name.'%');
        }

        if($active !== "activate")
            $qb->andWhere('r.isActive = 1');
        else
            $qb->andWhere('r.isActive = 0');

        return $qb->getQuery()->getResult();
    }


    private  function getOrCreateQueryBuilder( QueryBuilder $qb=null){
        return $qb?:$this->createQueryBuilder('r')
            ->orderBy('r.publishAt', 'DESC');
    }
}