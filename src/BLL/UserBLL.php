<?php

namespace App\BLL;

use App\Entity\User;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserBLL extends BaseBLL
{
    /** @var UserPasswordEncoderInterface $encoder
     */
    private $encoder;

    /**
     * @var JWTTokenManagerInterface
     */
    private $jwtManager;

    public function setJWTManager(
        JWTTokenManagerInterface $jwtManager)
    {
        $this->jwtManager = $jwtManager;
    }

    public function setEncoder(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function nuevo(
        string $username, string $password, string $email,string $avatar ,Request $request,
        $avatars_directory, $url_avatars_directory)
    {
        $user = new User();
        $user->setUsername($username);
        $user->setPassword(
            $this->encoder->encodePassword($user, $password)
        );
        $user->setEmail($email);
        $user->setAvatar($this->subirImagen($request, $avatar, $avatars_directory, $url_avatars_directory));

        return $this->guardaValidando($user);
    }

    public function getAll()
    {
        $users = $this->em->getRepository(User::class)
            ->findAll();

        return $this->entitiesToArray($users);
    }

    public function delete(User $user)
    {
        $this->deleteEntity($user);
    }

    public function profile()
    {
        $user = $this->getUser();

        return $this->toArray($user);
    }


    public function subirImagen(
        Request $request,
        $avatar,
        string $avatars_directory,
        string $url_avatars_directory)
    {
        $arr_avatar = explode(',', $avatar);

        if (count($arr_avatar) < 2)
            throw new BadRequestHttpException('formato de imagen incorrecto');

        $imgAvatar = base64_decode($arr_avatar[1]);
        if (!is_null($imgAvatar))
        {
            $fileName = 'restaurant-'.time().'.jpg';
            $filePath = $url_avatars_directory . $fileName;
            $urlAvatar = $request->getUriForPath($filePath);
            $ifp = fopen($avatars_directory . $fileName, "wb");
            if ($ifp)
            {
                $ok = fwrite($ifp, $imgAvatar);
                if ($ok)
                {
                    fclose($ifp);

                    return $urlAvatar;
                }
            }
        }

        return '/uploads/default.jpg';
    }







    public function cambiaAvatar(
        Request $request,
        string $avatar,
        string $avatars_directory,
        string $url_avatars_directory)
    {
        $user = $this->getUser();

        $arr_avatar = explode(',', $avatar);

        if (count($arr_avatar) < 2)
            throw new BadRequestHttpException('formato de imagen incorrecto');

        $imgAvatar = base64_decode($arr_avatar[1]);
        $user->setAvatar( $this->subirImagen($request, $avatar, $avatars_directory, $url_avatars_directory));
        return $this->guardaValidando($user);
        throw new \Exception('No se ha podido cargar la imagen del avatar');
    }

    public function cambiaPassword($nuevoPassword)
    {
        $user = $this->getUser();

        $user->setPassword(
            $this->encoder->encodePassword($user, $nuevoPassword));

        return $this->guardaValidando($user);
    }

    public function getTokenByEmail(string $email)
    {
        $user = $this->em->getRepository(User::class)
            ->findOneBy(['email'=>$email]);

        if (is_null($user))
            throw new AccessDeniedHttpException('Usuario no autorizado');

        return $this->jwtManager->create($user);
    }

    public function toArray($user)
    {
        if (is_null($user))
            return null;

        if (!($user instanceof User))
            throw new \Exception("La entidad no es un User");

        return [
            'id' => $user->getId(),
            'username' => $user->getUsername(),
            'email' => $user->getEmail(),
            'avatar' => $user->getAvatar()
        ];
    }
}