<?php
/**
 * Created by PhpStorm.
 * User: dwes
 * Date: 14/02/19
 * Time: 17:30
 */

namespace App\Controller\rest;

use App\BLL\UserBLL;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends BaseApiController
{
    /**
     * @Route(
     *      "/auth/register.{_format}", name="register",
     *      requirements={ "_format": "json" },
     *      defaults={"_format": "json"},
     *      methods={"POST"}
     * )
     */
    public function register(Request $request, UserBLL $userBLL)
    {
        $data = $this->getContent($request);
        $avatars_directory = $this->getParameter('avatars_directory');
        $url_avatars_directory = $this->getParameter('url_avatars_directory');

        $user = $userBLL->nuevo(
            $data['username'], $data['password'], $data['email'], $data['avatar'],$request,
            $avatars_directory,$url_avatars_directory);
        return $this->getResponse($user, Response::HTTP_CREATED);
    }

    /**
     * @Route(
     *     "/usuarios/{id}.{_format}",
     *     name="get_usuario",
     *      requirements={
     *          "id": "\d+",
     *          "_format": "json"
     *      },
     *      defaults={"_format": "json"},
     *      methods={"GET"}
     * )
     */
    public function getOne(User $user, UserBLL $userBLL)
    {
        return $this->getResponse($userBLL->toArray($user));
    }

    /**
     * @Route(
     *      "/usuarios.{_format}",
     *      name="get_usuarios",
     *      defaults={"_format": "json"},
     *      requirements={"_format": "json"},
     *      methods={"GET"}
     * )
     */
    public function getAll(UserBLL $userBLL)
    {
        $users = $userBLL->getAll();
        return $this->getResponse($users);
    }

    /**
     * @Route(
     *      "/usuarios/{id}.{_format}",
     *      name="delete_usuario",
     *      requirements={
     *          "id": "\d+",
     *          "_format": "json"
     *      },
     *      defaults={"_format": "json"},
     *      methods={"DELETE"}
     * )
     */
    public function delete(User $user, UserBLL $userBLL)
    {
        $userBLL->delete($user);

        return $this->getResponse(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * @Route(
     *      "/profile.{_format}",
     *      name="profile",
     *      requirements={
     *          "_format": "json"
     *      },
     *      defaults={"_format": "json"},
     *      methods={"GET"}
     * )
     */
    public function profile(UserBLL $userBLL)
    {
        $user = $userBLL->profile();

        return $this->getResponse($user);
    }

    /**
     * @Route(
     *      "/profile/avatar.{_format}", name="cambia_avatar",
     *      requirements={
     *          "_format": "json"
     *      },
     *      defaults={"_format": "json"},
     *      methods={"PATCH"}
     * )
     */
    public function cambiaAvatar(
        Request $request, UserBLL $userBLL)
    {
        $data = $this->getContent($request);

        if (is_null($data['avatar']))
            throw new BadRequestHttpException('No se ha recibido la imagen');

        $avatars_directory = $this->getParameter('avatars_directory');

        $url_avatars_directory = $this->getParameter('url_avatars_directory');

        $user = $userBLL->cambiaAvatar(
            $request, $data['avatar'], $avatars_directory, $url_avatars_directory);

        return $this->getResponse($user);
    }

    /**
     * @Route(
     *      "/profile/password.{_format}",
     *      name="cambia_password",
 *          requirements={
     *          "_format": "json"
     *      },
     *      defaults={"_format": "json"},
     *      methods={"PATCH"}
     * )
     */
    public function cambiaPassword(
        Request $request, UserBLL $userBLL)
    {
        $data = $this->getContent($request);

        if (is_null($data['password']) || !isset($data['password']) || empty($data['password']))
            throw new BadRequestHttpException('No se ha recibido el password');

        $user = $userBLL->cambiaPassword($data['password']);

        return $this->getResponse($user);
    }
}