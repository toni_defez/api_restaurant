<?php
/**
 * Created by PhpStorm.
 * User: tonid
 * Date: 22/02/2019
 * Time: 15:36
 */

namespace App\Controller\rest;


use App\BLL\RestaurantBLL;
use App\Entity\Restaurant;
use App\Entity\User;
use PhpParser\Comment;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RestaurantController extends BaseApiController
{

    /**
     * @Route(
     *      "/restaurant.{_format}", name="post_restaurant",
     *      defaults={"_format": "json"},
     *      requirements={"_format": "json"},
     *      methods={"POST"}
     * )
     */
    public function post(Request $request, RestaurantBLL $tareaBLL)
    {
        $data = $this->getContent($request);
        $avatars_directory = $this->getParameter('avatars_directory');
        $url_avatars_directory = $this->getParameter('url_avatars_directory');

        $tarea = $tareaBLL->nueva($data['name'], $data['description'], $data['daysOpen'],
            $data['cuisine'],$data['phone'],$data['image'],$request,
            $avatars_directory,$url_avatars_directory);

        return $this->getResponse($tarea, Response::HTTP_CREATED);
    }

    /**
     * @Route("/restaurant/{id}.{_format}", name="get_restaurant",
     * requirements={
     *      "id": "\d+",
     *      "_format": "json"
     * },
     * defaults={"_format": "json"},
     * methods={"GET"})
     */
    public function getOne(Restaurant $tarea, RestaurantBLL $tareaBLL)
    {
        return $this->getResponse($tareaBLL->toArray($tarea));
    }


    /**
     * @Route("/restaurants.{_format}", name="get_restaurants",
     *      defaults={"_format": "json"},
     *      requirements={"_format": "json"},
     *      methods={"GET"}
     * )
     * @Route("/restaurants/{order}", name="get_restaurants_order")
     */
    public function getAll(
        Request $request,
        RestaurantBLL $restaurantBLL,
        string $order='publishAt')
    {
        $name = $request->query->get('name');
        $numberStar = $request->query->get('numberStar');
        $open = $request->query->get('open');
        $active = $request->query->get('active');

        $tareas = $restaurantBLL->getRestaurantesFiltrados(
            $name, $numberStar, $open, $active,$order);

        return $this->getResponse($tareas);
    }


    /**
     * @Route("/restaurant/{id}.{_format}", name="update_restaurant",
     *      requirements={
     *          "id": "\d+",
     *          "_format": "json"
     *      },
     *      defaults={"_format": "json"},
     *      methods={"PUT"}
     * )
     */
    public function update(Request $request, Restaurant $restaurant, RestaurantBLL $restaurantBLL)
    {
        $data = $this->getContent($request);
        $avatars_directory = $this->getParameter('avatars_directory');
        $url_avatars_directory = $this->getParameter('url_avatars_directory');

        $tarea = $restaurantBLL->update($restaurant, $data,$request,
            $avatars_directory,$url_avatars_directory);

        return $this->getResponse($tarea, Response::HTTP_OK);
    }

    /**
     * @Route("/restaurant/{id}.{_format}", name="restaurant_tarea",
     *      requirements={
     *          "id": "\d+",
     *          "_format": "json"
     *      },
     *      defaults={"_format": "json"},
     *      methods={"DELETE"}
     * )
     */
    public function delete(Restaurant $restaurant, RestaurantBLL $restaurantBLL)
    {
        $response =  $restaurantBLL->delete($restaurant);

        return $this->getResponse(
            $response,
            Response::HTTP_NO_CONTENT
        );
    }
    /**
     * @Route("/restaurant/user/me.{_format}", name="get_restaurant_me",
     * requirements={
     *      "id": "\d+",
     *      "_format": "json"
     * },
     * defaults={"_format": "json"},
     * methods={"GET"})
     */
    public function getRestaurantMe( Request $request,
                                     RestaurantBLL $restaurantBLL){
        $tareas = $restaurantBLL->getRestaurantOwner();
        return $this->getResponse($tareas);
    }

    /**
     * @Route("/restaurant/user/{id}", name="get_restaurant_user",
     *      defaults={"_format": "json"},
     *      requirements={"_format": "json"},
     *      methods={"GET"}
     * )
     */
    public function getRestaurantByUser( User $user, RestaurantBLL $restaurantBLL){
        $tareas = $restaurantBLL->getRestaurantOwner($user);
        return $this->getResponse($tareas);
    }
}