<?php
/**
 * Created by PhpStorm.
 * User: tonid
 * Date: 22/02/2019
 * Time: 15:50
 */

namespace App\BLL;


use App\Entity\Comment;
use App\Entity\Restaurant;

class CommentBLL extends BaseBLL
{

    public function nueva(string $content , string $stars,Restaurant $restaurant){
        $comment = new Comment();
        $user  =$this->getUser();
        $comment->setAuthor($user);
        $comment->setContent($content);
        $comment->setStars(+$stars);
        $comment->setRestaurant($restaurant);
        return $this->guardaValidando($comment);
    }

    public function CommentsByRestaurant(Restaurant $restaurant){
        $tareas = $this->em->getRepository(Comment::class)->findBy(['restaurant'=>$restaurant]);
        return $this->entitiesToArray($tareas);
    }

    public function update(Comment $comment, array $data)
    {
        $comment->setContent($data['content']);
        $comment->setStars($data['stars']);
        return $this->guardaValidando($comment);

    }

    public function delete(Comment $comment)
    {
        try {
            $this->deleteEntity($comment);
            return [
                'error'=>false,
                'message'=>'Operation sucesfull , entity deleteded'];
        }
        catch (\Exception $exception){
            return [
                'error'=>true,
                'message'=>$exception->getMessage()];
        }
    }


    public function toArray($entity)
    {
        if (is_null($entity))
            return null;

        if (!($entity instanceof Comment))
            throw new Exception("La entidad no es una Comment");

        return [
            'id' => $entity->getId(),
            'content' => $entity->getContent(),
            'stars' => $entity->getStars()
        ];
    }
}

