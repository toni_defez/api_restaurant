<?php
/**
 * Created by PhpStorm.
 * User: tonid
 * Date: 22/02/2019
 * Time: 15:37
 */

namespace App\Controller\rest;


use App\BLL\CommentBLL;
use App\BLL\RestaurantBLL;
use App\Entity\Comment;
use App\Entity\Restaurant;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class CommentController extends BaseApiController
{

    /**
     * @Route(
     *      "/comment/{id}/new.{_format}", name="post_comment",
     *      defaults={"_format": "json"},
     *      requirements={"_format": "json"},
     *      methods={"POST"}
     * )
     */
    public function post(Restaurant $restaurant, Request $request, CommentBLL $tareaBLL)
    {
        $data = $this->getContent($request);
        $tarea = $tareaBLL->nueva($data['content'], $data['stars'],$restaurant);

        return $this->getResponse($tarea, Response::HTTP_CREATED);
    }

    /**
     *
     * @Route("/restaurant/{id}/comment/{idComment}.{_format}", name="get_comentario_restaurant",
     *     requirements={
     *          "id": "\d+",
     *          "_format": "json"
     *     },
     *     defaults={"_format": "json"},
     *     methods={"GET"}
     * )
     * @ParamConverter("comentario", options={"mapping": {"id":"producto", "idComment": "id"}})
     */
    public function getOneComment(Restaurant $producto, Comment $comentario, CommentBLL $comentarioBLL)
    {
        return $this->getResponse($comentarioBLL->toArray($comentario));
    }

    /**
     *
     * @Route("/restaurant/{id}/comment/{idComment}.{_format}", name="delete_comentario_restaurant",
     *     requirements={
     *          "id": "\d+",
     *          "_format": "json"
     *     },
     *     defaults={"_format": "json"},
     *     methods={"DELETE"}
     * )
     * @ParamConverter("comentario", options={"mapping": {"id":"producto", "idComment": "id"}})
     */
    public function deleteOneComment(Restaurant $producto, Comment $comentario, CommentBLL $comentarioBLL)
    {
        $comentarioBLL->deleteEntity($comentario);
        return $this->getResponse(null, Response::HTTP_NO_CONTENT);
    }


    /**
     * @Route("/comment/{id}.{_format}", name="update_comment",
     *      requirements={
     *          "id": "\d+",
     *          "_format": "json"
     *      },
     *      defaults={"_format": "json"},
     *      methods={"PUT"}
     * )
     */
    public function update(Request $request, Comment $comment, CommentBLL $commentBLL)
    {
        $data = $this->getContent($request);
        $comment = $commentBLL->update($comment, $data);
        return $this->getResponse($comment, Response::HTTP_OK);
    }

    /**
     * @Route("/comment/{id}.{_format}", name="delete_comment",
     *      requirements={
     *          "id": "\d+",
     *          "_format": "json"
     *      },
     *      defaults={"_format": "json"},
     *      methods={"DELETE"}
     * )
     */
    public function delete(Comment $comment, CommentBLL $commentBLL)
    {
        $response =  $commentBLL->delete($comment);

        return $this->getResponse(
            $response,
            Response::HTTP_NO_CONTENT
        );
    }
}